Package.describe({
  name: 'ubersoft:nouislider',
  version: '9.2.0',
  summary: 'Wrapper package for noUiSlider 9.2.0',
  git: 'https://bitbucket.org/ubersoftinc/meteor-nouislider.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.2.6');
  api.use('jquery', 'client');
  api.addFiles(['nouislider.min.js', 'nouislider.min.css'], 'client');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('jquery', 'client');
  api.use('ubersoft:nouislider');
  api.mainModule('nouislider-tests.js', 'client');
});
