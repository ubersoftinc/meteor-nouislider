# Example
html
```
<div id="slider"></div>
```
js
```
var slider = document.getElementById('slider')
window.noUiSlider.create(slider, {
  start: 20,
  connect: [true, false],
  range: {
    'min': 0,
    'max': 100
  }
})
```
