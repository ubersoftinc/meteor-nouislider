// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by nouislider.js.
import { name as packageName } from "meteor/ubersoft:nouislider";

// Write your tests here!
// Here is an example.
Tinytest.add('nouislider - example', function (test) {
  test.equal(packageName, "nouislider");
});
